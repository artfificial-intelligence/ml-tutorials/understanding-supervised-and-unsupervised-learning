# 지도학습과 비지도학습의 이해 <sup>[1](#footnote_1)</sup>

> <font size="3">지도 학습 방법과 비지도 학습 방법의 차이점을 알아본다.</font>

## 목차

1. [개요](./understanding.md#intro)
1. [지도 학습이란?](./understanding.md#sec_02)
1. [비지도 학습이란?](./understanding.md#sec_03)
1. [지도 학습과 비지도 학습 중에서 어떻게 선택할 것인가?](./understanding.md#sec_04)
1. [지도 및 비지도 학습 어플리케이션의 예](./understanding.md#sec_05)
1. [요약](./understanding.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 2 — Understanding Supervised and Unsupervised Learning](https://medium.com/datadriveninvestor/ml-tutorial-2-understanding-supervised-and-unsupervised-learning-2e7ef28a8070?sk=efb74478fbbd2d9b9d22a28e36693e0c)를 편역한 것입니다.
